# Ohcount - Docker container

This container builds the latest version of [Ohcount](https://github.com/blackducksoftware/ohcount) from source code.
Use it to obtain source code info with CIs (like Bitbucket Pipelines or similar).

The image is based on official Debian stable (slim version).

---

(C) 2018 Filippo Rigotto. Released under MIT License (see LICENSE file).
