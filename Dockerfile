FROM debian:stable-slim
#FROM resin/armv7hf-debian:stretch

LABEL maintainer="Filippo Rigotto <rigottofilippo@altervista.org>"
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /root

# Install dependencies
RUN apt-get -qq -y update 
RUN apt-get -qq -y install curl wget git-core
RUN apt-get -qq -y install build-essential ruby-dev rubygems
RUN apt-get -qq -y install libpcre3 libpcre3-dev libmagic-dev gperf gcc ragel swig
RUN gem install test-unit

# Clone and build
RUN git clone https://github.com/blackducksoftware/ohcount.git
WORKDIR /root/ohcount
RUN ./build ohcount
RUN mv bin/ohcount /usr/local/bin/ohcount

# External content location
WORKDIR /data
VOLUME ["/data"]
